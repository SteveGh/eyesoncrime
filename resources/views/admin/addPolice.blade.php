<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="shortcut icon" href="assets/images/favicon_1.ico">

    <title>Eyes on Crime - Admin Dashboard</title>

    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="assets/plugins/morris/morris.css">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="assets/js/modernizr.min.js"></script>
    <script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-69506598-1', 'auto');
        ga('send', 'pageview');
    </script>



</head>


<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left">
            <div class="text-center">
                <a href="" class="logo"><i class="icon-magnet icon-c-logo"></i><span>E<i class="md md-album"></i>C PANEL</span></a>

            </div>
        </div>

        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="">
                    <div class="pull-left">
                        <button class="button-menu-mobile open-left waves-effect waves-light">
                            <i class="md md-menu"></i>
                        </button>
                        <span class="clearfix"></span>
                    </div>

                    <ul class="nav navbar-nav navbar-right pull-right">

                        <li class="hidden-xs">
                            <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></a>
                        </li>
                        <li class="hidden-xs">
                            <a href="#" class="right-bar-toggle waves-effect waves-light"><i class="icon-settings"></i></a>
                        </li>
                        <li class="dropdown top-menu-item-xs">
                            <a href="#" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown"
                               aria-expanded="true"><img src="{!! Auth::guard('admin')->user()->url !!}" alt="user-img" class="img-circle"></span></span> </a>
                            <ul class="dropdown-menu">
                                <li><a href="{!! url('logout') !!}"><i class="ti-power-off m-r-10 text-danger"></i> Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div><!--/.nav-collapse -->
        </div>
    </div>
    <!-- Top Bar End -->

    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">
            <div class="user-details">
                <div class="pull-left">
                    <img src="{!! Auth::guard('admin')->user()->url !!}" alt="" class="thumb-md img-circle">
                </div>
                <div class="user-info">
                    <div class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">{!! Auth::guard('admin')->user()->name !!}<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0)"><i class="md md-face-unlock"></i> Profile<div class="ripple-wrapper"></div></a></li>
                            <li><a href="{!! url('userView') !!}"><i class="md md-settings"></i> Accounts</a></li>
                            <li><a href="{!! url('lockscreen') !!}"><i class="md md-lock"></i> Lock screen</a></li>
                            <li><a href="{!! url('logout') !!}"><i class="md md-settings-power"></i> Logout</a></li>
                        </ul>
                    </div>
                    <p class="text-muted m-0">Administrator</p>
                </div>
            </div>
            <!--- Divider -->
            <div id="sidebar-menu">
                <ul>

                    <li class="text-muted menu-title">Navigation</li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="ti-home"></i> <span> Complaints </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{!! url('complaints') !!}">View Complaints </a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="ti-paint-bucket"></i> <span> Crime Reports </span> <span class="menu-arrow"></span> </a>
                        <ul class="list-unstyled">
                            <li><a href="{!! url('Reported Photos') !!}">Photo</a></li>
                            <li><a href="javascript:void(0);">Video</a></li>
                            <li><a href="javascript:void(0);">Audio</a></li>
                            <li><a href="javascript:void(0);">Documents</a></li>
                            <li><a href="javascript:void(0);">Others</a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="{!! url('wanted') !!}" class="waves-effect"><i class="ti-light-bulb"></i><span class="label label-primary pull-right"></span><span> Wanted </span><span class="menu-arrow"></span> </a>
                    </li>

                    <li class="has_sub">
                        <a href="{!! url('policeOfficer') !!}" class="waves-effect"><i class="ti-spray"></i> <span> Police Officers </span> <span class="menu-arrow"></span> </a>
                    </li>

                    <li class="has_sub">
                        <a href="{!! url('report') !!}" class="waves-effect"><i class="ti-pencil-alt"></i><span> Reports </span> <span class="menu-arrow"></span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- ========== Left Sidebar End ========== -->

    <div class="content-page">

        <div class="content" >
            <div class="container">

                <h4 class="page-title">ADD POLICE OFFICER</h4>
                <ol class="breadcrumb">
                    <li><a href="">EOC</a></li><li><a href="#">Police Officers</a></li><li class="active">Add New Officer
                    </li>
                </ol>
            </div>
            <div class="col-lg-8 col-md-offset-1" >

                <div class="card-box">

                    <form action="{{url('/addPolice')}}" method="post"  data-parsley-validate novalidate enctype="multipart/form-data">
                        {{csrf_field()}}
                                <!-- Photo input-->
                        <div class="form-group">
                            <label for="fullName">Choose Photo</label>
                            <div class="form-group">
                                <img id="userPhoto"  src="assets/images/users/avatar-1.jpg" name="photo" alt="Your Image" class="img-responsive img-thumbnail" width="150"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <input id="photo" name="photo" onchange="document.getElementById('userPhoto').src = window.URL.createObjectURL(this.files[0])"  type="file" class="filestyle" data-buttonbefore="true">
                        </div>
                        <div class="form-group">
                            <label for="fullName">ID Number*</label>
                            <input type="text" name="officer_id" parsley-trigger="change" required placeholder="Enter Police ID" class="form-control" id="fullName">
                        </div>

                        <div class="form-group">
                            <label for="fullName">Rank*</label>
                            <select name="rank" class="form-control select2">
                                <option>Select</option>
                                <optgroup label="Junior Officer">
                                <option>Constable</option>
                                <option>Lance Corporal (L/Cpl)</option>
                                <option>Corporal (Cpl)</option>
                                <option>Sergeant</option>
                                <option>District Sergeant Major (DSM)</option>
                                <option>Regiment Sergeant Major (RSM)</option>
                                <option>Inspector (Inspr)</option>
                                <option>Chief Inspector (C/Inspr)</option>
                                    </optgroup>
                                <optgroup label="Senior Officer">
                                    <option>Assistant Superintendent of Police (ASP)</option>
                                    <option>Deputy Superintendent of Police (DSP)</option>
                                    <option>Superintendent of Police (Supt)</option>
                                    <option>Chief Superintendent of Police (C/Supt)</option>
                                    <option>Assistant Commissioner of Police (ACP)</option>
                                    <option>Deputy Commissioner of Police (DCOP)</option>
                                    <option>Commissioner of Police (COP)</option>
                                </optgroup>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="fullName">First Name*</label>
                            <input type="text" name="fname" parsley-trigger="change" required placeholder="Enter first name" class="form-control" id="fullName">
                        </div>

                        <div class="form-group">
                            <label for="fullName">Last Name*</label>
                            <input type="text" name="lname" parsley-trigger="change" required placeholder="Enter last name" class="form-control" id="fullName">
                        </div>

                        <div class="form-group">
                            <label for="fullName">Gender*</label>
                            <select name="gender" class="form-control select2">
                                <option>Select</option>
                                <option>Male</option>
                                <option>Female</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="fullName">Date of Birth*</label>
                            <div class="input-group">
                                <input name="dob" type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose">
                                <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                            </div><!-- input-group -->
                        </div>

                        <div class="form-group">
                            <label for="fullName">Specialty*</label>
                            <select name="specialty" class="form-control select2">
                                <option>Select</option>
                                    <option>Anti-terrorism</option>
                                    <option>Homicide</option>
                                    <option>Crime Investigation</option>
                                    <option>Cyber crimes</option>
                                    <option>Latent Print Examiner</option>
                                    <option>Sketch Artist</option>
                                    <option>Community Relations Officer</option>
                                    <option>Covert operations & intelligence</option>
                            </select>
                        </div>

                        <div class="form-group text-right m-b-0">
                            <button class="btn btn-primary waves-effect waves-light" type="submit">
                                Submit
                            </button>
                            <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">
                                Cancel
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- container -->
    </div>

</div>


<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/detect.js"></script>
<script src="assets/js/fastclick.js"></script>
<script src="assets/js/jquery.slimscroll.js"></script>
<script src="assets/js/jquery.blockUI.js"></script>
<script src="assets/js/waves.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>

<script src="assets/plugins/moment/moment.js"></script>
<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
<script src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="assets/plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>


<script src="assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>


<script src="assets/js/jquery.core.js"></script>
<script src="assets/js/jquery.app.js"></script>


<script type="text/javascript" src="assets/plugins/parsleyjs/parsley.min.js"></script>
<script src="assets/pages/jquery.form-pickers.init.js"></script>


<script type="text/javascript">
    $(document).ready(function() {
        $('form').parsley();
    });
</script>

<script>
    function readURL(input) {

        for(i=0; i< input.files.length; i++) {


            if (input.files && input.files[i]) {


                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview').html('<img src ="' + e.target.result + '">');
                };

                reader.readAsDataURL(input.files[i]);
            }
        }
    }
</script>

<script>
    $(function () {
        $(":file").change(function () {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
            }
        });
    });

    function imageIsLoaded(e) {
        $('photo').attr('src', e.target.result);
    }

</script>

</body>
</html>