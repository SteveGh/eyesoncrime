<?php use Illuminate\Support\Facades\Input; ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="shortcut icon" href="assets/images/favicon_1.ico">

    <title>Eyes on Crime - Admin Dashboard</title>

    <!--venobox lightbox-->
    <link rel="stylesheet" href="assets/plugins/magnific-popup/css/magnific-popup.css"/>

    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="assets/js/modernizr.min.js"></script>
    <script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-69506598-1', 'auto');
        ga('send', 'pageview');
    </script>


</head>


<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper">


    <!-- Top Bar Start -->
    <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left">
            <div class="text-center">
                <a href="" class="logo"><i class="icon-magnet icon-c-logo"></i><span>E<i class="md md-album"></i>C PANEL</span></a>

            </div>
        </div>

        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="">
                    <div class="pull-left">
                        <button class="button-menu-mobile open-left waves-effect waves-light">
                            <i class="md md-menu"></i>
                        </button>
                        <span class="clearfix"></span>
                    </div>

                    <ul class="nav navbar-nav navbar-right pull-right">

                        <li class="hidden-xs">
                            <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></a>
                        </li>
                        <li class="hidden-xs">
                            <a href="#" class="right-bar-toggle waves-effect waves-light"><i class="icon-settings"></i></a>
                        </li>
                        <li class="dropdown top-menu-item-xs">
                            <a href="#" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown"
                               aria-expanded="true"><img src="{!! Auth::guard('admin')->user()->url !!}" alt="user-img" class="img-circle"></span></span> </a>
                            <ul class="dropdown-menu">
                                <li><a href="{!! url('logout') !!}"><i class="ti-power-off m-r-10 text-danger"></i> Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div><!--/.nav-collapse -->
        </div>
    </div>
    <!-- Top Bar End -->

    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">
            <div class="user-details">
                <div class="pull-left">
                    <img src="{!! Auth::guard('admin')->user()->url !!}" alt="" class="thumb-md img-circle">
                </div>
                <div class="user-info">
                    <div class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">{!! Auth::guard('admin')->user()->name !!}<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0)"><i class="md md-face-unlock"></i> Profile<div class="ripple-wrapper"></div></a></li>
                            <li><a href="{!! url('userView') !!}"><i class="md md-settings"></i> Accounts</a></li>
                            <li><a href="javascript:void(0)"><i class="md md-lock"></i> Lock screen</a></li>
                            <li><a href="{!! url('logout') !!}"><i class="md md-settings-power"></i> Logout</a></li>
                        </ul>
                    </div>
                    <p class="text-muted m-0">Administrator</p>
                </div>
            </div>
            <!--- Divider -->
            <div id="sidebar-menu">
                <ul>

                    <li class="text-muted menu-title">Navigation</li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="ti-home"></i> <span> Complaints </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{!! url('complaints') !!}">View Complaints </a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="ti-paint-bucket"></i> <span> Crime Reports </span> <span class="menu-arrow"></span> </a>
                        <ul class="list-unstyled">
                            <li><a href="{!! url('Reported Photos') !!}">Photo</a></li>
                            <li><a href="javascript:void(0);">Video</a></li>
                            <li><a href="javascript:void(0);">Audio</a></li>
                            <li><a href="javascript:void(0);">Documents</a></li>
                            <li><a href="javascript:void(0);">Others</a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="{!! url('wanted') !!}" class="waves-effect"><i class="ti-light-bulb"></i><span class="label label-primary pull-right"></span><span> Wanted </span><span class="menu-arrow"></span> </a>
                    </li>

                    <li class="has_sub">
                        <a href="{!! url('policeOfficer') !!}" class="waves-effect"><i class="ti-spray"></i> <span> Police Officers </span> <span class="menu-arrow"></span> </a>
                    </li>

                    <li class="has_sub">
                        <a href="{!! url('report') !!}" class="waves-effect"><i class="ti-pencil-alt"></i><span> Reports </span> <span class="menu-arrow"></span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- ========== Left Sidebar End ========== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Settings <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
                        </div>
                        <ol class="breadcrumb">
                            <li><a href="#">EOC</a></li>
                            <li><a href="#">Crime Report</a></li>
                            <li class="active">Gallery</li>
                        </ol>
                    </div>
                </div>

                <!-- SECTION FILTER
                ================================================== -->
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 ">
                        <div class="portfolioFilter">
                            <a href="#" data-filter="*"


                               @if(Input::get('type')== "all")
                               class="current"
                               @endif

                                    >All</a>
                            <a href="#" data-filter=".photo"
                               @if(Input::get('type')== "image")
                               class="current"
                                    @endif
                                    >Photos of crimes</a>
                            <a href="#" data-filter=".video"
                               @if(Input::get('type')== "video")
                               class="current"
                                    @endif
                                    >Videos</a>
                            <a href="{{url('/Reported%20Photos?type=audio')}}" data-filter=".audio"
                               @if(Input::get('type')== "audio")
                               class="current"
                                    @endif
                                    >Audio</a>
                            <a href="#" data-filter=".document"
                               @if(Input::get('type')== "other")
                               class="current"
                                    @endif
                                    >Other Documents</a>
                        </div>
                    </div>
                </div>
            </div> <!-- container -->


            @if(\Illuminate\Support\Facades\Input::get('type') == "all" || !\Illuminate\Support\Facades\Input::has('type') )
                <?php $count = 1; ?>
                @foreach($all as $item)
                    <div class="col-md-4">
                        <a href="{{url($item)}}">Download File {{$count}} </a>
                    </div>
                    <?php $count++; ?>
                @endforeach

            @endif


            @if(\Illuminate\Support\Facades\Input::get('type') == "image")

            @foreach($images as $item)
                <div class="col-md-4">
                    <img src="{{url($item)}}" class="img img-responsive">
                </div>
            @endforeach

            @endif

            @if(\Illuminate\Support\Facades\Input::get('type') == "audio")

            @foreach($audios as $item)
                <div class="col-md-6">
                    <audio src="{{url($item)}}" controls ></audio>
                </div>
            @endforeach

            @endif


            @if(\Illuminate\Support\Facades\Input::get('type') == "video")

                @foreach($audios as $item)
                    <div class="col-md-6">
                        <video src="{{url($item)}}"  controls ></video>
                    </div>
                @endforeach

            @endif


        </div> <!-- content -->

        <footer class="footer">
            � 2017. All rights reserved.
        </footer>

    </div>
</div>
<!-- END wrapper -->

<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/detect.js"></script>
<script src="assets/js/fastclick.jss"></script>
<script src="assets/js/jquery.slimscroll.js"></script>
<script src="assets/js/jquery.blockUI.js"></script>
<script src="assets/js/waves.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>


<script src="assets/js/jquery.core.js"></script>
<script src="assets/js/jquery.app.js"></script>

<script type="text/javascript" src="assets/plugins/isotope/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="assets/plugins/magnific-popup/js/jquery.magnific-popup.min.js"></script>

<script type="text/javascript">
    $(window).load(function(){
        var $container = $('.portfolioContainer');
        $container.isotope({
            filter: '*',
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });

        $('.portfolioFilter a').click(function(){
            $('.portfolioFilter .current').removeClass('current');
            $(this).addClass('current');

            var selector = $(this).attr('data-filter');
            $container.isotope({
                filter: selector,
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });
            return false;
        });
    });
    $(document).ready(function() {
        $('.image-popup').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-fade',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0,1] // Will preload 0 - before current, and 1 after the current image
            }
        });
    });
</script>

</body>
</html>