<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Input;

Route::get('/','Adminauth\AuthController@showFormLogin');
Route::post('login','Adminauth\AuthController@login');

Route::group(['middleware'=>['admin']],function(){
    Route::get('/dashboard','Admin\AdminController@dashboard');
    Route::get('/logout','Adminauth\AuthController@logout');
});

Route::post('adduser','Admin\AdminController@postUserForm');
Route::post('addWanted','Admin\AdminController@postWantedForm');
Route::post('addPolice','Admin\AdminController@postPoliceForm');


Route::get('/create', function(){
    App\User::create([
        'name'=>'Zibit',
        'username'=>'zibit',
        'email'=>'zibit@gmail.com',
        'password'=>bcrypt('1234'),
    ]);
});

Route::get('/userView', function(){
    $users = \App\users::all();
    return view('admin.user', [
        'users'=>$users
    ]);

});

Route::get('/newStaff', function(){
    return view('admin.adduser');
});

Route::get('/policeOfficer', function(){
    $users = \App\police::all();
    return view('admin.policeOfficersView', [
        'users'=>$users
    ]);
});

Route::get('/complaints', function(){
    $users = \App\caseComplaint::all();
    return view('admin.complaintsView', [
        'users'=>$users
    ]);
});

Route::get('/Reported Photos', function(){
    $users = \App\crimeReport::all();

    $images = array();
    $videos = array();
    $audios = array();
    $all = array();

    if(Input::has('type'))
    $type = Input::get("type");
    else
        $type = "all";

    if($type == "image") {

        $file = new File_Iterator_Factory();
        $images = $file->getFileIterator('./crimes_reported/upload/', 'jpeg');
    }

    else if($type == "video") {

        $file = new File_Iterator_Factory();
        $videos = $file->getFileIterator('./crimes_reported/upload/', 'mp4');
    }

    else if($type == "audio") {

        $file = new File_Iterator_Factory();
        $audios = $file->getFileIterator('./crimes_reported/upload/', 'mp3');
    }

    else {

        $file = new File_Iterator_Factory();
        $all = $file->getFileIterator('./crimes_reported/upload/');
    }


    return view('admin.crimeReportedPhotos', [
        'users'=>$users,
        'images' => $images,
        'videos' =>$videos,
        'audios' => $audios,
        'all' => $all
    ]);
});

Route::get('/report', function(){
    return view('admin.reports');
});

Route::get('/wanted', function(){
    $users = \App\wantedPerson::all();
    return view('admin.wantedView', [
        'users'=>$users
    ]);
});

Route::get('/AddWanted', function(){
    return view('admin.addWantedPerson');
});

Route::get('/lockscreen',function(){
    return view('admin.lockscreen');
});

Route::get('/addPolice',function(){
    return view('admin.addPolice');
});

