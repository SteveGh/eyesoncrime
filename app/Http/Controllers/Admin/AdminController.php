<?php

namespace App\Http\Controllers\Admin;

use App\police;
use App\users;
use App\wantedPerson;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public  function _construct(){
        $this->middleware('admin');
    }
    public function dashboard(){
        return view('admin.layout');
    }


    public function postUserForm(Request $request){
        $url = $request->file('photo')->getClientOriginalName();
        $request->file('photo')->move('uploads',$url);
        $user = new users();
        $user->url = url('/uploads') ."/" . $url;
        $user->name = $request->input('name');
        $user->username = $request->input('username');
        $user->email = $request->input('email');
        $user->password = $request->input('password');
        $user->save();

        return view('admin.adduser',['status' => 'New User successfully added']);
    }

    public function postWantedForm(Request $request){
        $url = $request->file('photo')->getClientOriginalName();
        $request->file('photo')->move('wanted_uploads',$url);
        $wanted = new wantedPerson();
        $wanted->url = url('/wanted_uploads') ."/" . $url;
        $wanted->category = $request->input('category');
        $wanted->fname = $request->input('fname');
        $wanted->gender = $request->input('gender');
        $wanted->age = $request->input('age');
        $wanted->description = $request->input('description');
        $wanted->save();

        return view('admin.addWantedPerson',['status' => 'New User successfully added']);
    }

    public function postPoliceForm(Request $request){
        $url = $request->file('photo')->getClientOriginalName();
        $request->file('photo')->move('police_uploads',$url);
        $policeOfficer = new police();
        $policeOfficer->url = url('/police_uploads') ."/" . $url;
        $policeOfficer->rank = $request->input('rank');
        $policeOfficer->fname = $request->input('fname');
        $policeOfficer->lname = $request->input('lname');
        $policeOfficer->gender = $request->input('gender');
        $policeOfficer->dob = $request->input('dob');
        $policeOfficer->officer_id = $request->input('officer_id');
        $policeOfficer->specialty = $request->input('specialty');
        $policeOfficer->save();

        return view('admin.addPolice',['status' => 'New User successfully added']);
    }

}
