<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class users extends Model
{

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

}
